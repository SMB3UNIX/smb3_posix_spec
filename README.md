# Build Instructions

1. Install pandoc

2. Install [wkhtmltopdf](https://github.com/wkhtmltopdf/packaging/releases/0.12.6-1)

3. Run `make`

The build creates 2 artifacts named smb3_posix_extensions.html and
smb3_posix_extensions.pdf.