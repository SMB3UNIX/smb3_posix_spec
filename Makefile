all: smb3_posix_extensions.pdf

smb3_posix_extensions.pdf: smb3_posix_extensions.html
	# Requires wkhtmltopdf with patched qt
	wkhtmltopdf --footer-right "[page] / [topage]" --footer-left "SMB3 POSIX Extensions" --footer-line --footer-font-name "Segoe UI" --footer-font-size 10 smb3_posix_extensions.html smb3_posix_extensions.pdf

smb3_posix_extensions.html: smb3_posix_extensions.md
	pandoc --from=markdown_mmd -Vcss= -Vpagetitle="SMB3 POSIX Extensions" --standalone --to=html smb3_posix_extensions.md >$@

smb3_posix_extensions_with_toc.html: smb3_posix_extensions.md
	pandoc --from=markdown_mmd -Vcss= -Vpagetitle="SMB3 POSIX Extensions" --standalone --toc --toc-depth=4 --to=html smb3_posix_extensions.md >$@

clean:
	rm smb3_posix_extensions.pdf smb3_posix_extensions.html >/dev/null 2>&1 || echo
